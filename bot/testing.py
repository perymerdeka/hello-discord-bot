# import random

import discord
import os
from dotenv import load_dotenv
from discord.ext import commands

load_dotenv('.env')
bot = commands.Bot(command_prefix="$")

class ClientBot(discord.Client):
    # bot command
    @commands.command()
    async def ping(self, ctx):
        print(f'Bot: {self.user} has Online From Command')
        await ctx.channel.send("Test")

    # check if bot slide to server
    async def on_ready(self):
        print(f'Bot: {self.user} has Online')

    # new member welcome handling
    async def on_member_join(self, member):
        guild = member.guild
        if guild.system_channel is not None:
            send_message = f'Halo, {member.mention} selamat datang dan selamat bergabung, silahkan memperkenalkan diri'
            await guild.system_channel.send(send_message)

# intents members
intents = discord.Intents.default()
intents.members = True

# run bot
client = ClientBot(intents=intents, command_prefix='$')
client.run(os.getenv('TOKEN'))