### Simple Bot Discord

this is project to learn how to make discord bot using python with discord.py library

#### Library Documentations

---

* discord.py: https://discordpy.readthedocs.io/en/latest/
* pypi project: https://pypi.org/project/discord.py/

### Run Project

##### Create Virtual Environtment
after clone project enter this command to create new virtual environtment
```commandline
python -m discord_bot_env
```


##### Activate
**On Windows**
```bash
discord_bot_env\Scripts\activate.bat
```

**on linux**
```commandline
source discord_bot_env/bin/activate
```

##### Install Requirements
```bash
pip install -r requirements.txt
```

##### Create `.env` file to store your token

after create insert your token to `.env` file

example: 
```buildoutcfg
TOKEN=S0m3ToK312
```
> Note: get Token From: https://discord.com/developers/applications

### Run File `main.py`

```bash
python main.py
```
then check you discord bot
