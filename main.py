import discord
import os

from discord.ext import commands
from dotenv import load_dotenv
from scraper import get_quote
import random


# define discord client
client = discord.Client()
load_dotenv('.env')

# Token Config
TOKEN = os.getenv('TOKEN')



@client.event
async def on_ready():
    print(f'we have login as {client.user}')


# handle if receive command
@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.content.startswith('$hello'):
        await message.channel.send('Hello Master {} nice to meet you'.format(message.author))

    if message.content.startswith('$quote'):
        quote = get_quote()
        await message.channel.send(quote)
        await message.reply(quote, mention_author=True)


if __name__ == '__main__':
    client.run(os.getenv('TOKEN'))
