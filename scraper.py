import requests
import json
url = 'https://zenquotes.io/api/random'

headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36',
}

def get_quote():
    res = requests.get(url, headers=headers)
    print('Connection Status: {}'.format(res.status_code))

    # getting content
    json_data = json.loads(res.text)
    quote = json_data[0]['q'] + " - " + json_data[0]['a']
    if res.status_code != 200:
        quote = "i can't find the master quote"

    return quote
